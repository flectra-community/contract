# Flectra Community / contract

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[agreement_serviceprofile](agreement_serviceprofile/) | 2.0.1.1.1| Adds an Agreement Service Profile object
[contract_payment_mode](contract_payment_mode/) | 2.0.1.1.2| Payment mode in contracts and their invoices
[contract_update_last_date_invoiced](contract_update_last_date_invoiced/) | 2.0.1.0.0|         This module allows to update the last date invoiced if invoices are deleted.
[agreement_maintenance](agreement_maintenance/) | 2.0.1.2.0| Manage maintenance agreements and contracts
[agreement_sale](agreement_sale/) | 2.0.1.0.0| Agreement on sales
[agreement_tier_validation](agreement_tier_validation/) | 2.0.1.0.1| Extends the functionality of Agreement to support a tier validation process.
[agreement_stock](agreement_stock/) | 2.0.1.0.1| Link picking to an agreement
[contract_sale_tag](contract_sale_tag/) | 2.0.1.0.1|         Allows to transmit contract tags to sale order (through sale_generation)
[product_contract](product_contract/) | 2.0.1.2.0| Recurring - Product Contract
[agreement_legal_sale](agreement_legal_sale/) | 2.0.1.0.1| Create an agreement when the sale order is confirmed
[contract_sale](contract_sale/) | 2.0.1.2.0| Contract from Sale
[agreement_project](agreement_project/) | 2.0.1.0.0| Link projects to an agreement
[agreement_mrp](agreement_mrp/) | 2.0.1.0.0| Link manufacturing orders to an agreement
[agreement_repair](agreement_repair/) | 2.0.1.0.0| Link repair orders to an agreement
[subscription_oca](subscription_oca/) | 2.0.1.0.1| Generate recurring invoices.
[agreement_account](agreement_account/) | 2.0.1.0.0| Agreement on invoices
[contract_variable_qty_prorated](contract_variable_qty_prorated/) | 2.0.1.0.0|         This module adds a formula to compute prorated quantity to invoice as        extension of the module contract_variable_quantity
[agreement](agreement/) | 2.0.1.1.1| Adds an agreement object
[contract_delivery_zone](contract_delivery_zone/) | 2.0.1.0.0|         Allows to remind the delivery zone defined on the partner on contract level.
[contract_invoice_start_end_dates](contract_invoice_start_end_dates/) | 2.0.1.0.0| Contract Invoice Start End Dates
[contract_split](contract_split/) | 2.0.1.0.0| Split contract
[contract_sale_generation](contract_sale_generation/) | 2.0.1.0.2| Contracts Management - Recurring Sales
[agreement_legal](agreement_legal/) | 2.0.2.4.3| Manage Agreements, LOI and Contracts
[contract_mandate](contract_mandate/) | 2.0.1.0.1| Mandate in contracts and their invoices
[contract_queue_job](contract_queue_job/) | 2.0.1.0.0|         This addon make contract invoicing cron plan each contract in a job        instead of creating all invoices in one transaction
[contract](contract/) | 2.0.2.14.0| Recurring - Contracts Management
[contract_variable_quantity](contract_variable_quantity/) | 2.0.1.0.0| Variable quantity in contract recurrent invoicing


