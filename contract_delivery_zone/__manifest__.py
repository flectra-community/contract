# Copyright 2022 ACSONE SA/NV
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Contract Delivery Zone",
    "summary": """
        Allows to remind the delivery zone defined on the partner on contract level.""",
    "version": "2.0.1.0.0",
    "license": "AGPL-3",
    "author": "ACSONE SA/NV,Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/contract",
    "depends": [
        "contract",
        "partner_delivery_zone",
    ],
    "data": ["views/contract_contract.xml"],
}
