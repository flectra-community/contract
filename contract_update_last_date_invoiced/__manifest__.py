# Copyright 2022 ACSONE SA/NV
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Contract Last Date Update",
    "summary": """
        This module allows to update the last date invoiced if invoices are deleted.""",
    "version": "2.0.1.0.0",
    "license": "AGPL-3",
    "author": "ACSONE SA/NV,Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/contract",
    "depends": ["contract"],
    "data": [
        "security/security.xml",
        "views/contract_line.xml",
        "wizards/update_last_date_invoiced.xml",
    ],
}
