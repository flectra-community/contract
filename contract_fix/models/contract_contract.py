import logging

from flectra import models, fields, api, _
from flectra.osv import expression

_logger = logging.getLogger(__name__)


class ContractContract(models.Model):
    _inherit = 'contract.contract'

    @api.onchange("contract_template_id")
    def _onchange_contract_template_id(self):
        """Update the contract fields with that of the template.

        Take special consideration with the `contract_line_ids`,
        which must be created using the data from the contract lines. Cascade
        deletion ensures that any errant lines that are created are also
        deleted.
        """
        contract_template_id = self.contract_template_id
        if not contract_template_id:
            return
        for field_name, field in contract_template_id._fields.items():
            if field.name == "contract_line_ids":
                lines = self._convert_contract_lines(contract_template_id)
                self.contract_line_ids = lines
            elif field.name == 'contract_line_fixed_ids':
                continue
            elif not any(
                    (
                            field.compute,
                            field.related,
                            field.automatic,
                            field.readonly,
                            field.company_dependent,
                            field.name in self.NO_SYNC,
                    )
            ):
                if self.contract_template_id[field_name]:
                    self[field_name] = self.contract_template_id[field_name]

    def _convert_contract_lines(self, contract):
        """
        Overwrite method to show lines directly after template selection
        """
        self.ensure_one()
        new_lines = [(5, 0, 0)]
        for contract_line in contract.contract_line_ids:
            vals = contract_line._convert_to_write(contract_line.read()[0])
            # Remove template link field
            vals.pop("contract_template_id", False)
            vals.pop("contract_id", False)
            vals.pop("id", False)
            vals["date_start"] = self.date_start
            vals["recurring_next_date"] = self.recurring_next_date
            new_lines.append((0, 0, vals))
        return new_lines

    @api.model_create_multi
    def create(self, vals_list):
        for vals in vals_list:
            vals.pop('contract_line_fixed_ids', False)
        records = super().create(vals_list)
        records._update_lines_from_contract()
        return records

    def write(self, vals):
        result = super(ContractContract, self).write(vals)
        self._update_lines_from_contract()
        return result

    def _update_lines_from_contract(self):
        lines = self.mapped('contract_line_ids')
        lines._set_recurrence_field('recurring_rule_type')
        lines._set_recurrence_field('recurring_invoicing_type')
        lines._set_recurrence_field('recurring_interval')
        lines._set_recurrence_field('date_start')
        lines._set_recurrence_field('recurring_next_date')
        lines._set_recurrence_field('date_end')

    @api.depends()
    def _compute_recurring_next_date(self):
        result = super(ContractContract, self)._compute_recurring_next_date()
        for contract in self.filtered(lambda f: f.date_end):
            if len(contract.contract_line_ids) > 0:
                last_date_invoiced = min(contract.contract_line_ids.mapped('last_date_invoiced'))
                if last_date_invoiced and last_date_invoiced >= contract.date_end:
                    contract.recurring_next_date = False
        return result

    @api.model
    def _cron_recurring_create(self, date_ref=False, create_type="invoice"):
        """
        The cron function in order to create recurrent documents
        from contracts.
        """
        _recurring_create_func = self._get_recurring_create_func(
                create_type=create_type
        )
        if not date_ref:
            date_ref = fields.Date.context_today(self)
        domain = self._get_contracts_to_invoice_domain(date_ref)
        domain = expression.AND(
                [
                    domain,
                    [("generation_type", "=", create_type)],
                ]
        )
        contracts = self.search(domain)
        companies = set(contracts.mapped("company_id"))
        # Invoice by companies, so assignation emails get correct context
        for company in companies:
            contracts_to_invoice = contracts.filtered(
                    lambda c: c.company_id == company
            ).with_company(company)
            _recurring_create_func(contracts_to_invoice, date_ref)
        return True
